require_relative 'ten_pin'

puts 'Please enter scores with space separating each score e.g. 1 1 10'
puts 'Invalid input will be ignored'
puts 'CMD or CTRL + X to exit'

while true
  puts "=> #{TenPin.compute_score(gets)}"
end
