require_relative '../ten_pin'

RSpec.describe TenPin do

  let(:input) { '' }
  subject { TenPin.compute_score(input) }

  context "with no strikes" do
    let(:input) { '1 2 3 4' }
    it { is_expected.to eq(10) }
  end

  context 'with spares' do
    let(:input) { '9 1 9 1' }
    it { is_expected.to eq(29) }
  end

  context 'with a strike' do
    let(:input) { '1 1 1 1 10 1 1' }
    it { is_expected.to eq(18) }
  end

  context 'with strikes 'do
    let(:input) { '1 1 10 10 1 1' }
    it { is_expected.to eq(37) }
  end

  context 'with strikes and spares' do
    let(:input) { '9 1 10 9 1' }
    it { is_expected.to eq(50) }
  end

  context 'with all strikes' do
    let(:input) { '10 10 10 10 10 10 10 10 10 10 10 10' }
    it { is_expected.to eq(300) }
  end
end
