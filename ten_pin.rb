class TenPin
  def self.compute_score(input)
    new(input).compute_score
  end

  def initialize(input)
    @input = input
    @score_frames = split_input_to_frames(input)
  end

  def compute_score
    @score_frames.each.with_index.reduce(0) do |total, (frame, index)|
      return total if index >= 10
      if strike?(frame)
        ball_1, ball_2 = get_next_n_balls(index, 2)
        total + frame.first + ball_1.to_i + ball_2.to_i
      elsif spare?(frame)
        next_ball = get_next_n_balls(index, 1).first
        total + frame.first + frame.last + next_ball.to_i
      else
        total + frame.first.to_i + frame.last.to_i
      end
    end
  end

  private

  def split_input_to_frames(input)
    input.split(/\s/).map(&:to_i).each_with_object([]) do |score, frames|
      last_frame = frames.last
      if last_frame && last_frame.size == 1 && last_frame.first != 10
        last_frame << score
      else
        frames << [score]
      end
    end
  end

  def get_next_n_balls(frame_index, n)
    balls = []
    index = frame_index + 1
    while true
      frame = @score_frames[index]
      break unless frame
      balls = balls.concat(frame)
      break if balls.size >= n
      index += 1
    end
    balls.take(n)
  end

  def strike?(frame)
    frame.size == 1 && frame.first == 10
  end

  def spare?(frame)
    frame.first + (frame.last || 0) == 10
  end
end
